const navigation = document.querySelector(".navigation__list");
const menuBtn = document.querySelector(".menu");
const closeNav = document.querySelector(".icon__x");

const btnScrollCate = document.querySelector(".btn--scroll-cate");
const btnScrollContact = document.querySelector(".btn--scroll-contact");
const sectionCate = document.querySelector(".section__categories");

const sectionContact = document.querySelector(".footer");

// menuBtn.addEventListener("click", () => {
//   navigation.classList.toggle("show-nav");
// });

menuBtn.addEventListener("click", () => navigation.classList.add("show-nav"));

closeNav.addEventListener("click", () => navigation.classList.remove("show-nav"));

const allLink = document.querySelectorAll(".navigation__link");
console.log(allLink);

allLink.forEach((link) =>
  link.addEventListener("click", function (e) {
    e.preventDefault();
    console.log("get link");
    const id = this.getAttribute("href");
    console.log(id);
    document.querySelector(id).scrollIntoView({ behavior: "smooth" });
  })
);

document.querySelector(".navigation__list").addEventListener("click", function (e) {
  e.preventDefault();

  if (e.target.classList.contains("navigation__link")) {
    const id = e.target.getAttribute("href");
    document.querySelector(id).scrollIntoView({ behavior: "smooth" });
  }
});
